/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package unittest;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Nobpharat
 */
public class OXUnitTest {

    public OXUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinRow01_X_output_true() {
        char[][] table = {{'X', 'X', 'X'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 1;
        boolean result = OX_game_Func.checkWin(table, row, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow02_X_output_true() {
        char[][] table = {{'-', '-', '-'}, {'X', 'X', 'X'}, {'O', '-', 'O'}};
        char currentPlayer = 'X';
        int row = 2;
        boolean result = OX_game_Func.checkWin(table, row, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow03_X_output_true() {
        char[][] table = {{'-', 'O', 'O'}, {'-', '-', '-'}, {'X', 'X', 'X'}};
        char currentPlayer = 'X';
        int row = 3;
        boolean result = OX_game_Func.checkWin(table, row, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol01_X_output_true() {
        char[][] table = {{'X', '-', '-'}, {'X', 'O', '-'}, {'X', '-', 'O'}};
        char currentPlayer = 'X';
        int col = 1;
        boolean result = OX_game_Func.checkWin(table, col, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol02_X_output_true() {
        char[][] table = {{'O', 'X', 'O'}, {'-', 'X', '-'}, {'-', 'X', '-'}};
        char currentPlayer = 'X';
        int col = 2;
        boolean result = OX_game_Func.checkWin(table, col, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol03_X_output_true() {
        char[][] table = {{'-', 'O', 'X'}, {'-', 'O', 'X'}, {'-', '-', 'X'}};
        char currentPlayer = 'X';
        int col = 3;
        boolean result = OX_game_Func.checkWin(table, col, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinDiagonal_X_output_true() {
        char[][] table = {{'X', '-', '-'}, {'O', 'X', 'O'}, {'-', '-', 'X'}};
        char currentPlayer = 'X';
        int col = 3;
        boolean result = OX_game_Func.checkWin(table, col, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckdraw_X_output_true() {
        char[][] table = {{'X', 'O', 'X'}, {'O', 'O', 'X'}, {'X', 'X', 'O'}};
        boolean result = OX_game_Func.checkDraw(table);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow01_O_output_true() {
        char[][] table = {{'O', 'O', 'O'}, {'X', 'X', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1;
        boolean result = OX_game_Func.checkWin(table, row, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow02_O_output_true() {
        char[][] table = {{'X', 'X', '-'}, {'O', 'O', 'O'}, {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2;
        boolean result = OX_game_Func.checkWin(table, row, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow03_O_output_true() {
        char[][] table = {{'-', '-', '-'}, {'X', 'X', '-'}, {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int row = 3;
        boolean result = OX_game_Func.checkWin(table, row, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol01_O_output_true() {
        char[][] table = {{'O', 'X', '-'}, {'O', 'X', '-'}, {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        boolean result = OX_game_Func.checkWin(table, col, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol02_O_output_true() {
        char[][] table = {{'-', 'O', 'X'}, {'-', 'O', 'X'}, {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;
        boolean result = OX_game_Func.checkWin(table, col, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol03_O_output_true() {
        char[][] table = {{'X', '-', 'O'}, {'X', '-', 'O'}, {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;
        boolean result = OX_game_Func.checkWin(table, col, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinDiagonal_left_O_output_true() {
        char[][] table = {{'O', '-', '-'}, {'X', 'O', 'X'}, {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;
        boolean result = OX_game_Func.checkWin(table, col, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinDiagonal_Right_O_output_true() {
        char[][] table = {{'-', '-', 'O'}, {'X', 'O', 'X'}, {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 3;
        boolean result = OX_game_Func.checkWin(table, col, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckdraw_O_output_true() {
        char[][] table = {{'O', 'X', 'O'}, {'X', 'X', 'O'}, {'O', 'O', 'X'}};
        boolean result = OX_game_Func.checkDraw(table);
        assertEquals(true, result);
    }

    //out of scenarios
    @Test
    public void testCheckWinRow01_X_output_false() {
        char[][] table = {{'X', 'X', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 1;
        boolean result = OX_game_Func.checkWin(table, row, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow02_X_output_false() {
        char[][] table = {{'-', '-', '-'}, {'X', 'X', '-'}, {'O', '-', 'O'}};
        char currentPlayer = 'X';
        int row = 2;
        boolean result = OX_game_Func.checkWin(table, row, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow03_X_output_false() {
        char[][] table = {{'-', 'O', 'O'}, {'-', '-', '-'}, {'X', 'X', '-'}};
        char currentPlayer = 'X';
        int row = 3;
        boolean result = OX_game_Func.checkWin(table, row, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol01_X_output_false() {
        char[][] table = {{'X', '-', '-'}, {'X', 'O', '-'}, {'O', '-', 'O'}};
        char currentPlayer = 'X';
        int col = 1;
        boolean result = OX_game_Func.checkWin(table, col, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol02_X_output_false() {
        char[][] table = {{'O', 'X', 'O'}, {'-', 'O', '-'}, {'-', 'X', '-'}};
        char currentPlayer = 'X';
        int col = 2;
        boolean result = OX_game_Func.checkWin(table, col, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol03_X_output_false() {
        char[][] table = {{'-', 'O', 'X'}, {'-', 'O', 'X'}, {'-', '-', '-'}};
        char currentPlayer = 'X';
        int col = 3;
        boolean result = OX_game_Func.checkWin(table, col, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinDiagonal_X_output_false() {
        char[][] table = {{'X', '-', '-'}, {'O', 'X', 'O'}, {'-', '-', 'O'}};
        char currentPlayer = 'X';
        int col = 3;
        boolean result = OX_game_Func.checkWin(table, col, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckdraw_X_output_false() {
        char[][] table = {{'X', 'O', 'X'}, {'O', 'O', 'X'}, {'X', 'X', '-'}};
        boolean result = OX_game_Func.checkDraw(table);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow01_O_output_false() {
        char[][] table = {{'O', 'O', '-'}, {'X', 'X', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1;
        boolean result = OX_game_Func.checkWin(table, row, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow02_O_output_false() {
        char[][] table = {{'-', '-', '-'}, {'X', 'X', '-'}, {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int row = 2;
        boolean result = OX_game_Func.checkWin(table, row, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinRow03_O_output_false() {
        char[][] table = {{'-', 'O', 'O'}, {'-', '-', '-'}, {'X', 'X', '-'}};
        char currentPlayer = 'O';
        int row = 3;
        boolean result = OX_game_Func.checkWin(table, row, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol01_O_output_false() {
        char[][] table = {{'O', '-', '-'}, {'O', 'X', '-'}, {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 1;
        boolean result = OX_game_Func.checkWin(table, col, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol02_O_output_false() {
        char[][] table = {{'-', 'O', 'X'}, {'-', 'O', 'X'}, {'-', '-', '-'}};
        char currentPlayer = 'O';
        int col = 2;
        boolean result = OX_game_Func.checkWin(table, col, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinCol03_O_output_false() {
        char[][] table = {{'X', '-', 'O'}, {'X', '-', 'O'}, {'-', '-', 'X'}};
        char currentPlayer = 'O';
        int col = 3;
        boolean result = OX_game_Func.checkWin(table, col, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckWinDiagonal_left_O_output_false() {
        char[][] table = {{'O', '-', '-'}, {'X', '-', 'X'}, {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;
        boolean result = OX_game_Func.checkWin(table, col, currentPlayer);
        assertEquals(false, result);
    }

    @Test
    public void testCheckdraw_O_output_false() {
        char[][] table = {{'O', 'X', 'O'}, {'X', 'X', 'O'}, {'O', 'O', '-'}};
        boolean result = OX_game_Func.checkDraw(table);
        assertEquals(false, result);
    }

}
