/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package unittest;

import java.util.Scanner;

/**
 *
 * @author Nobpharat
 */
public class OX_game_Func {

    public static void main(String[] args) {
        char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 1;
        int col = 1;

        while (true) {
            printWelcome();
            printTable(table);
            while (true) {
                printTurn(currentPlayer);
                inputRowCol(table, row, col, currentPlayer);
                printTable(table);
                if (checkWin(table, col, currentPlayer)) {
                    printWinner(currentPlayer);
                    break;
                }
                if (checkDraw(table)) {
                    printDraw();
                    break;
                }
                currentPlayer=changePlayer(currentPlayer);
            }
            if (!isAgain()) {
                break;
            }
        }

    }

    private static void printWelcome() {
        System.out.println("Welcome to XO");
    }

    private static void printTable(char[][] table) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private static void printTurn(char currentPlayer) {
        System.out.println(currentPlayer + " Turn");
    }

    private static void inputRowCol(char[][] table, int row, int col, char currentPlayer) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input row,col : ");
        while (true) {
            row = sc.nextInt();
            col = sc.nextInt();
            if (row > 0 && row < 4 && col > 0 && col < 4) {
                if (table[row - 1][col - 1] == '-') {
                    table[row - 1][col - 1] = currentPlayer;
                    return;
                }
            }
            System.out.print("Invalid input. Please enter row[1-3] col[1-3] : ");
        }
    }

    private static char changePlayer(char currentPlayer) {
    if (currentPlayer == 'X') {
        return 'O';
    } else {
        return 'X';
    }
}

    private static void printWinner(char currentPlayer) {
        System.out.println(currentPlayer + " is a Winner!");
    }

    public static boolean checkWin(char[][] table, int rowcol, char currentPlayer) {
        if (checkRow(table, rowcol, currentPlayer) || checkCol(table, rowcol, currentPlayer) || checkDiagonal(table, currentPlayer)) {
            return true;
        }
        return false;
    }

    public static boolean checkDraw(char[][] table) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] != 'X' && table[i][j] != 'O') {
                    return false;
                }
            }
        }
        return true;

    }

    private static boolean checkRow(char[][] table, int rowcol, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (table[rowcol - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol(char[][] table, int rowcol, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (table[i][rowcol - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDiagonal(char[][] table, char currentPlayer) {
        if ((table[0][0] == currentPlayer && table[0][0] == table[1][1] && table[1][1] == table[2][2])
                || (table[1][1] == currentPlayer && table[1][1] == table[0][2] && table[1][1] == table[2][0])) {
            return true;
        }
        return false;
    }

    private static void printDraw() {
        System.out.println("Game is a draw!");
    }

    private static boolean isAgain() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input continue (y/n)  : ");
        while (true) {
            String ans = sc.next();
            if (ans.equals("y") || ans.equals("n")) {
                if (ans.equals("y")) {
                    return true;
                } else {
                    return false;
                }
            }
            System.out.println("to start the game. Please choose y or n.");
        }
    }

}
