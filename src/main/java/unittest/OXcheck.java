/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package unittest;

/**
 *
 * @author Nobpharat
 */
public class OXcheck {
    public static boolean checkWin(char[][] table, int rowcol, char currentPlayer) {
        if (checkRow(table, rowcol, currentPlayer) || checkCol(table, rowcol, currentPlayer) || checkDiagonal(table, currentPlayer)) {
            return true;
        }
        return false;
    }

    public static boolean checkDraw(char[][] table) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] != 'X' && table[i][j] != 'O') {
                    return false;
                }
            }
        }
        return true;

    }

    private static boolean checkRow(char[][] table, int rowcol, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (table[rowcol - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol(char[][] table, int rowcol, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (table[i][rowcol - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDiagonal(char[][] table, char currentPlayer) {
        if ((table[0][0] == currentPlayer && table[0][0] == table[1][1] && table[1][1] == table[2][2])
                || (table[1][1] == currentPlayer && table[1][1] == table[0][2] && table[1][1] == table[2][0])) {
            return true;
        }
        return false;
    }
}
